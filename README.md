# mineman explosion resistance override (1.18.2)

runtime override block explosion resistances using Unsafe reflection.

specific to version: 1.18.2