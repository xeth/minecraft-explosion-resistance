/**
 * Unsafe reflection to replace mineman explosion resistance at runtime.
 * 
 * NOTE DIFFERENCE BETWEEN BUKKIT BLAST RESISTANCE AND REAL BLAST RESISTANCE:
 * When calling `material.getExplosionResistance()`, these are hard-coded
 * values in the Bukkit Material class:
 * https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/browse/src/main/java/org/bukkit/Material.java#9195
 * 
 * But these are NOT the actual explosion resistances used in server.
 * Actual explosion resistance is attached to BlockBehaviour class,
 * https://nms.screamingsandals.org/1.18.2/net/minecraft/world/level/block/state/BlockBehaviour.html
 * 
 */

package xeth.explosionresistance

import sun.misc.Unsafe
import java.io.File
import java.util.logging.Logger
import java.util.EnumMap
import net.minecraft.core.Registry
import net.minecraft.world.level.block.state.BlockBehaviour
import org.bukkit.craftbukkit.v1_18_R2.util.CraftMagicNumbers
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import net.kyori.adventure.text.Component


internal interface MessageSender: Plugin {
    // colors
    val colorInfo: ChatColor
    val colorError: ChatColor

    // print generic message to chat
    public fun print(sender: CommandSender?, s: String) {
        if ( sender === null ) {
            this.logger.severe("Message called with null sender: ${s}")
            return
        }

        val msg = Component.text("${colorInfo}${s}")
        sender.sendMessage(msg)
    }

    // print error message to chat
    public fun error(sender: CommandSender?, s: String) {
        if ( sender === null ) {
            this.logger.severe("Message called with null sender: ${s}")
            return
        }

        val msg = Component.text("${colorError}${s}")
        sender.sendMessage(msg)
    }
}

public class ExplosionResistanceCommand(val plugin: ExplosionResistancePlugin) : CommandExecutor, TabCompleter {

    override fun onCommand(sender: CommandSender, cmd: Command, commandLabel: String, args: Array<String>): Boolean {
        
        // no args, open flags gui
        if ( args.size == 0 ) {
            this.printHelp(sender)
            return true
        }

        // parse subcommand
        val arg = args[0].lowercase()
        when ( arg ) {
            "help" -> printHelp(sender)
            "list" -> list(sender)
            "revert" -> revert(sender)
            "reload" -> reload(sender)
        }

        return true
    }

    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<String>): List<String> {
        return listOf("help", "list", "revert", "reload")
    }

    private fun printHelp(sender: CommandSender?) {
        plugin.print(sender, "[ExplosionResistance] block explosion resistance overrides for Mineman 1.18.2")
        return
    }

    /**
     * Revert all explosion resistances
     */
    private fun revert(sender: CommandSender?) {
        val player = if ( sender is Player ) sender else null
        if ( player === null || player.isOp() ) {
            plugin.revertAll()
            plugin.print(sender, "[ExplosionResistance] reverted all overrides")
        }
        else {
            plugin.error(sender, "[ExplosionResistance] Only operators can reload")
        }
    }

    /**
     * Reload config
     */
    private fun reload(sender: CommandSender?) {
        val player = if ( sender is Player ) sender else null
        if ( player === null || player.isOp() ) {
            plugin.loadConfig()
            plugin.print(sender, "[ExplosionResistance] reloaded")
        }
        else {
            plugin.error(sender, "[ExplosionResistance] Only operators can reload")
        }
    }

    /**
     * List overrides
     */
    private fun list(sender: CommandSender?) {
        for ( blockOverride in plugin.overrides.values ) {
            val nmsBlock = CraftMagicNumbers.getBlock(blockOverride.material)
            val actual = nmsBlock.getExplosionResistance()
            val oldVal = blockOverride.oldValue
            val newVal = blockOverride.newValue
            plugin.print(sender, "${blockOverride.material}: ${actual} (${oldVal} -> ${newVal})")
        }
    }
}

/**
 * Block explosion resistance override.
 */
internal data class BlockExplosionResistance(
    val material: Material,
    val oldValue: Float,
    val newValue: Float,
)

/**
 * Manager for replaceing block explosion resistance with unsafe reflection.
 */
private class BlockExplosionResistanceReflection {
    // unsafe reflection
    private val unsafe: Unsafe

    // runtime field for explosion resistance
    // https://nms.screamingsandals.org/1.18.2/net/minecraft/world/level/block/state/BlockBehaviour.html
    private val fieldExplosionResistance = BlockBehaviour::class.java.getDeclaredField("aH")
    
    // runtime field for explosion resistance inside BlockProperties
    // apparently we dont need to change this...
    // https://nms.screamingsandals.org/1.18.2/net/minecraft/world/level/block/state/BlockBehaviour$Properties.html
    // private val fieldBlockProperties = BlockBehaviour::class.java.getDeclaredField("aO")
    // private val fieldBlockPropertiesExplosionResistance = BlockBehaviour.Properties::class.java.getDeclaredField("f")

    init {
        val unsafeGetter = Unsafe::class.java.getDeclaredField("theUnsafe")
        unsafeGetter.setAccessible(true)
        this.unsafe = unsafeGetter.get(null) as Unsafe

        // // use to see runtime obfuscated fields
        // for ( f in BlockBehaviour::class.java.getDeclaredFields() ) {
        //     println("BlockBehaviour.${f.name}: $f")
        // }
        // for ( f in BlockBehaviour.Properties::class.java.getDeclaredFields() ) {
        //     println("BlockBehaviour.Properties.${f.name}: $f")
        // }
    }

    /**
     * Use unsafe reflection to replace the explosion resistance of a nms
     * block at runtime. Returns the old value.
     */
    fun replace(material: Material, newValue: Float): BlockExplosionResistance {
        // https://hub.spigotmc.org/stash/projects/SPIGOT/repos/craftbukkit/browse/src/main/java/org/bukkit/craftbukkit/util/CraftMagicNumbers.java#158
        val nmsBlock = CraftMagicNumbers.getBlock(material)
        val oldValue = nmsBlock.getExplosionResistance()
        unsafe.putFloat(nmsBlock, unsafe.objectFieldOffset(fieldExplosionResistance), newValue)

        // apparently we don't need to set the blockProperties, 
        // not used for explosion resistance
        // val blockProperties = unsafe.getObject(nmsBlock, unsafe.objectFieldOffset(fieldBlockProperties)) as BlockBehaviour.Properties
        // println("blockProperties: $blockProperties")
        // println("blockProperties.explosionResistance: ${unsafe.getFloat(blockProperties, unsafe.objectFieldOffset(fieldBlockPropertiesExplosionResistance))}")
        // println("blockProperties.destroyTime: ${unsafe.getFloat(blockProperties, unsafe.objectFieldOffset(fieldBlockPropertiesDestroyTime))}")
        // unsafe.putFloat(blockProperties, unsafe.objectFieldOffset(fieldBlockPropertiesDestroyTime), 5.0f)

        return BlockExplosionResistance(
            material=material,
            oldValue=oldValue,
            newValue=newValue,
        )
    }
}

public class ExplosionResistancePlugin : JavaPlugin(), MessageSender {

    // block overrides
    // use enum map to avoid any duplicate overrides
    internal var overrides: EnumMap<Material, BlockExplosionResistance> = EnumMap(Material::class.java)

    // colors for message sending
    override val colorInfo = ChatColor.DARK_GREEN
    override val colorError = ChatColor.RED
    
    /**
     * Revert explosion resistances back to old values.
     */
    public fun revertAll() {
        val explosionResistanceReflection = BlockExplosionResistanceReflection()
        for ( blockOverride in this.overrides.values ) {
            explosionResistanceReflection.replace(blockOverride.material, blockOverride.oldValue)
        }

        this.overrides = EnumMap(Material::class.java)
    }

    /**
     * Load config, update settings
     * Returns number of flags loaded
     */
    public fun loadConfig() {
        // revert existing changes
        val explosionResistanceReflection = BlockExplosionResistanceReflection()
        for ( blockOverride in this.overrides.values ) {
            explosionResistanceReflection.replace(blockOverride.material, blockOverride.oldValue)
        }
        this.overrides = EnumMap(Material::class.java)
        
        // load config file
        val configFile = File(this.getDataFolder().getPath(), "config.yml")
        if ( !configFile.exists() ) {
            this.logger.info("No config found: generating default config.yml")
            this.saveDefaultConfig()
        }

        val config = YamlConfiguration.loadConfiguration(configFile)

        // load materials
        val materialNames = config.getKeys(false)
        for ( matName in materialNames ) {
            try {
                val mat = Material.matchMaterial(matName)
                if ( mat === null ) {
                    this.logger.warning("Invalid material name: ${matName}")
                    continue
                }
            
                val newExplosionResistance = config.getDouble(matName).toFloat()
                // this.logger.info("Setting ${mat} explosion resistance to ${newExplosionResistance} (default ${mat.getBlastResistance()})") // verbose

                val blockOverride = explosionResistanceReflection.replace(mat, newExplosionResistance)

                // check for duplicate override
                val existingOverride = this.overrides[mat]
                if ( existingOverride !== null ) {
                    this.logger.severe("Duplicate material in config: ${mat} (old: ${existingOverride.newValue}, new: ${newExplosionResistance})")
                    this.overrides[mat] = BlockExplosionResistance(
                        material=mat,
                        oldValue=existingOverride.oldValue, // use existing value's old value so we don't lose track
                        newValue=blockOverride.newValue,
                    )
                } else {
                    this.overrides[mat] = blockOverride
                }

                // verify
                val nmsBlock = CraftMagicNumbers.getBlock(mat)
                if ( nmsBlock.getExplosionResistance() != newExplosionResistance ) {
                    this.logger.severe("Failed to set ${mat} explosion resistance to ${newExplosionResistance} (currently is ${nmsBlock.getExplosionResistance()})")
                }

            }
            catch ( e: Exception ) {
                e.printStackTrace()
            }
        }
    }

    override fun onEnable() {
        // measure load time
        val timeStart = System.currentTimeMillis()

        // load flags
        this.loadConfig()

        // register commands
        this.getCommand("explosionresistance")?.setExecutor(ExplosionResistanceCommand(this))
        
        // print load time
        val timeEnd = System.currentTimeMillis()
        val timeLoad = timeEnd - timeStart
        logger.info("Enabled in ${timeLoad}ms")

        // print success message
        logger.info("now this is epic")
    }

    override fun onDisable() {
        this.revertAll()
        logger.info("wtf i hate xeth now")
    }
}
